chrome.runtime.onInstalled.addListener(() => {
    chrome.storage.sync.get(null, function (savedSettings) {
        if(!savedSettings.accounts)  {
            const accounts = [{
                tabid: '1',
                accountId: null,
                logo: null
            }]
            chrome.storage.sync.set({ accounts });
        }
    })
});