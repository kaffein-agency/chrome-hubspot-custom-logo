const DEBUG = true;

chrome.storage.sync.get(
    {
        accounts: [
            {
                accountId: "",
                logo: "",
            },
        ],
    },
    function (savedSettings) {
        const account = savedSettings.accounts.find((account) =>
            new RegExp('hubspot\.com\/.+' + account.accountId ).test(window.location.href)
        );
        log("Got storage, checking if we should init");
        log({account, href: window.location.href});

        account && account.accountId && account.logo && init(account);
    }
);

/**
 * Initialize the script for a given account
 *
 * @param {object} account
 */
const init = (account) => {
    log("init");

    const container = document.createElement("div");
    container.style.position = "relative";
    container.style.height = "50px";
    container.style.width = "50px";

    const logo = document.createElement("img");
    logo.src = account.logo;
    logo.style.position = "absolute";
    logo.style.height = "100%";

    container.appendChild(logo);

    const interval = setInterval(() => {
        const hubspotLogo = document.querySelector('.nav-logo');
        if(hubspotLogo) {
            hubspotLogo.replaceWith(container)
            document.querySelector('#hs-nav-v4--logo').replaceWith(container.cloneNode(true))
            clearInterval(interval);
        }
    });
};

/**
 *
 * @param {string} message
 */
const log = (message) => {
    DEBUG && console.log(message);
}