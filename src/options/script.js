$(function () {


    //add new Tabs
    function addTabMenu(imenuTab, savedSettings = null) {
        var imenuTabText = "tab-menu-"+imenuTab;
        var imenuTabBodyText = "tab-body-"+imenuTab;
        var $tabMenu = $('.tab-account-menu:last').clone(true);
        $('.tab-account-menu:last').find('button').removeClass('active').attr('aria-selected', false);
        $tabMenu.find('button').attr('id', imenuTabText).attr('data-bs-target', "#"+imenuTabBodyText).attr('aria-controls', imenuTabText);
        $tabMenu.find("#tab-menu-"+imenuTab).text("Draft");
        $tabMenu.insertAfter(".tab-account-menu:last");

        //Clone Content Body
        var $tabContent = $('.tab-account-body:last').clone(true);
        $tabContent.find('input[name="tabid"]').val(imenuTab);
        $tabContent.find('input[name="accountId"]').val(savedSettings?.accountId ?? "");
        $('.tab-account-body:last').removeClass('show active');
        $tabContent.attr('id', imenuTabBodyText).attr('aria-labelledby', imenuTabText);
        $tabContent.find('form').attr("id", "form-content-"+imenuTab);
        $tabContent.find('.save-form').attr("tab-content-id", imenuTab);
        $tabContent.find('.btn-add').attr("tab-content-id", imenuTab);
        $tabContent.find('.status-current-form').text("");
        $tabContent.insertAfter(".tab-account-body:last");
    }
    var imenuTab = 0;
    var settingsStorageArray = [];

    // Restores select box and checkbox state using the preferences
    // stored in chrome.storage.
    chrome.storage.sync.get({
        accounts:[{
            accountId: '',
            logo: ''
        }],
    }, function (data) {
        settingsStorageArray = data.accounts;
        imenuTab = settingsStorageArray.length;
        console.log(settingsStorageArray);
        for (let i = 0; i < settingsStorageArray.length; i++) {
            var savedSettings = settingsStorageArray[i];
            var currentId = savedSettings.tabid;
            var parentcontent = "#form-content-"+currentId;
            var imenuTabBodyText = "#tab-body-"+currentId;
            //ADD MENU
            if(i > 0) {
                addTabMenu(currentId, savedSettings)
            }
            $(parentcontent+ ' #hubspot_account_id').val(savedSettings?.accountId ?? "");
            const tabName = $('.tab-account-menu').find("#tab-menu-"+currentId);
            tabName.text(savedSettings.accountId ?? "Draft");

            if(savedSettings.logo) {
                $(parentcontent).find('.existingLogo').html('<img src="'+savedSettings.logo+'" width="100" height="100" />');
            }
        }
    });

    $('#addplus-tab').off('click').on('click', function () {
        imenuTab++;
        addTabMenu(imenuTab);
    });

    $('#myTabContent').on("click", ".save-form", function () {
        var currentId = $(this).attr("tab-content-id");
        var formCurrent =$('form[id=form-content-'+currentId+']');
        const accountSettings = formCurrent.serializeForm()

        const logoBlob = formCurrent.find('#logo')?.[0]?.files?.[0]

        console.log({logoBlob});

        if(!logoBlob) {
            console.log("without logo")
            saveAccountSettings(settingsStorageArray, accountSettings, currentId);
        }
        else {
            console.log("with logo")

            const config = {
                quality: 0.5,
                maxWidth: 100,
                maxHeight: 100,
                debug: true
            };

            BrowserImageResizer.readAndCompressImage(logoBlob, config)
                .then(resizedBlob => {

                    var a = new FileReader();
                    a.onload = function(e) {
                        accountSettings.logo = e.target.result
                        $("#form-content-"+currentId + " .existingLogo").html('<img src="'+accountSettings.logo+'" width="100" height="100" />')
                        saveAccountSettings(settingsStorageArray, accountSettings, currentId);
                    }
                    a.readAsDataURL(resizedBlob);


                })
        }
    })
})


function saveAccountSettings(settingsStorageArray, accountSettings, currentId) {
    const parentContent = "#form-content-"+currentId;

    console.log(accountSettings);

    const status = $(parentContent +' .status-current-form');
    const tabName = $('.tab-account-menu').find("#tab-menu-"+currentId);
    const index = settingsStorageArray.findIndex((e) => (e.accountId === accountSettings.accountId || parseInt(e.tabid) === parseInt(accountSettings.tabid)));
    if (index === -1) {
        settingsStorageArray.push(accountSettings);
    } else {
        settingsStorageArray[index] = accountSettings;
    }
    chrome.storage.sync.set({accounts: settingsStorageArray}, function () {
        status.text('Options sauvegardées');
        tabName.text(accountSettings.accountId);
        setTimeout(function () {
            status.textContent = '';
        }, 5000);
    });
}

$.fn.serializeForm = function () {
    const _ = {};
    let pop;
    $.map(this.serializeArray(), function (n) {
        const keys = n.name.match(/[a-zA-Z0-9_]+|(?=\[])/g);
        if (keys.length > 1) {
            let tmp = _;
            pop = keys.pop();
            for (let i = 0; i < keys.length, j = keys[i]; i++) {
                tmp[j] = (!tmp[j] ? (pop === '') ? [] : {} : tmp[j]);
                tmp = tmp[j];
            }
            if (pop === '') {
                tmp = (!Array.isArray(tmp) ? [] : tmp);
                tmp.push(n.value);
            } else {
                tmp[pop] = n.value;
            }
        } else {
            _[keys.pop()] = n.value;
        }
    });
    return _;
}
